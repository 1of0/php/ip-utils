<?php

namespace OneOfZero\IpUtils\Test;

use OneOfZero\IpUtils\WellKnown;
use PHPUnit\Framework\TestCase;

class WellKnownTest extends TestCase
{
    public function testGetIpv4PrivateRanges(): void
    {
        $this->assertTrue(WellKnown::getIpv4PrivateRanges()->contains('10.0.0.0/8'));
        $this->assertTrue(WellKnown::getIpv4PrivateRanges()->contains('172.16.0.0/12'));
        $this->assertTrue(WellKnown::getIpv4PrivateRanges()->contains('192.168.0.0/16'));
    }

    public function testGetIpv4LinkLocalRanges(): void
    {
        $this->assertTrue(WellKnown::getIpv4LinkLocalRanges()->contains('169.254.0.0/16'));
    }

    public function testGetIpv4MulticastRanges(): void
    {
        $this->assertTrue(WellKnown::getIpv4MulticastRanges()->contains('224.0.0.0/4'));
    }

    public function testGetIpv4LocalHostRanges(): void
    {
        $this->assertTrue(WellKnown::getIpv4LocalHostRanges()->contains('127.0.0.0/8'));
    }
}