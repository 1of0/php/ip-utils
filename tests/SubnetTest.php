<?php

/**
 * SPDX-License-Identifier: MIT
 * Find the full license text at: https://gitlab.com/1of0/php/ip-utils/-/blob/master/LICENSE.md
 */

namespace OneOfZero\IpUtils\Test;

use InvalidArgumentException;
use OneOfZero\IpUtils\BinaryString;
use OneOfZero\IpUtils\IpAddress;
use OneOfZero\IpUtils\NetworkLocationInterface;
use OneOfZero\IpUtils\ParseException;
use OneOfZero\IpUtils\Subnet;

class SubnetTest extends AbstractTest
{
    /**
     * @dataProvider getValidCidrProvider
     * @param string $cidr
     */
    public function testIsValidCidrValid($cidr): void
    {
        $this->assertTrue(Subnet::isValidCidr($cidr), "{$cidr} was marked as invalid CIDR");
    }

    /**
     * @dataProvider getInvalidCidrProvider
     * @param string $cidr
     */
    public function testIsValidCidrInvalid($cidr): void
    {
        $this->assertFalse(Subnet::isValidCidr($cidr), "{$cidr} was marked as valid CIDR");
    }

    /**
     * @dataProvider getValidCidrProvider
     * @param string $cidr
     * @param string $expectedIp
     * @param int $expectedPrefixLength
     */
    public function testParseCidrValid(string $cidr, string $expectedIp, int $expectedPrefixLength): void
    {
        $subnet = Subnet::parseCidr($cidr);
        $this->assertInstanceOf(Subnet::class, $subnet);
        $this->assertEquals($expectedPrefixLength, $subnet->getPrefixLength());
        $this->assertEquals($expectedIp, $subnet->getNetworkAddress()->toString());
    }

    /**
     * @dataProvider getInvalidCidrProvider
     * @param string $cidr
     */
    public function testParseCidrInvalid($cidr): void
    {
        $this->expectException(ParseException::class);
        Subnet::parseCidr($cidr);
    }

    /**
     * @dataProvider getValidPrefixLengthProvider
     * @param string $ip
     * @param int $prefixLength
     */
    public function testConstructorValid(string $ip, int $prefixLength): void
    {
        $this->assertNotNull(new Subnet(IpAddress::parse($ip), $prefixLength));
    }

    /**
     * @dataProvider getInvalidPrefixLengthProvider
     * @param string $ip
     * @param int $prefixLength
     */
    public function testConstructorInvalid(string $ip, int $prefixLength): void
    {
        $this->expectException(ParseException::class);
        new Subnet(IpAddress::parse($ip), $prefixLength);
    }

    /**
     * @dataProvider getValidCidrProvider
     * @param string $cidr
     */
    public function testDebugInfo(string $cidr): void
    {
        $subnet = Subnet::parseCidr($cidr);
        $this->assertEquals($cidr, $subnet->__debugInfo()['cidr']);
    }

    /**
     * @dataProvider getValidCidrProvider
     * @param string $cidr
     * @param string $ip
     * @param int $prefixLength
     */
    public function testGetIdentifier(string $cidr, string $ip, int $prefixLength): void
    {
        $ipAddress = IpAddress::parse($ip);
        $prefixMask = BinaryString::createMaskForPrefixLength(
            $prefixLength,
            IpAddress::ADDRESS_LENGTH_BITS[$ipAddress->getAddressFamily()]
        );

        $this->assertEquals(
            "{$ipAddress->getIdentifier()}{$prefixMask->toHex()}",
            Subnet::parseCidr($cidr)->getIdentifier()
        );
    }

    /**
     * @dataProvider getValidContainsProvider
     * @param string $cidr
     * @param string $ip
     */
    public function testContainsValid(string $cidr, string $ip): void
    {
        $this->assertTrue(Subnet::parseCidr($cidr)->contains(IpAddress::parse($ip)));
    }

    /**
     * @dataProvider getInvalidContainsProvider
     * @param string $cidr
     * @param string $ip
     */
    public function testContainsInvalid(string $cidr, string $ip): void
    {
        $this->assertFalse(Subnet::parseCidr($cidr)->contains(IpAddress::parse($ip)));
    }

    public function testContainsInvalidType(): void
    {
        $mockLocation = $this->createMock(NetworkLocationInterface::class);
        $this->assertFalse(Subnet::parseCidr('192.168.0.0/16')->contains($mockLocation));
    }

    /**
     * @dataProvider getValidContainsSubnetProvider
     * @param string $parentCidr
     * @param string $childCidr
     */
    public function testContainsSubnetValid(string $parentCidr, string $childCidr): void
    {
        $this->assertTrue(Subnet::parseCidr($parentCidr)->containsSubnet(Subnet::parseCidr($childCidr)));
        $this->assertTrue(Subnet::parseCidr($parentCidr)->contains(Subnet::parseCidr($childCidr)));
    }

    /**
     * @dataProvider getInvalidContainsSubnetProvider
     * @param string $parentCidr
     * @param string $childCidr
     */
    public function testContainsSubnetInvalid(string $parentCidr, string $childCidr): void
    {
        $this->assertFalse(Subnet::parseCidr($parentCidr)->containsSubnet(Subnet::parseCidr($childCidr)));
        $this->assertFalse(Subnet::parseCidr($parentCidr)->contains(Subnet::parseCidr($childCidr)));
    }

    public function testEquals(): void
    {
        $a = Subnet::parseCidr('192.168.0.0/16');
        $b = Subnet::parseCidr('192.168.0.0/16');
        $c = Subnet::parseCidr('192.168.0.0/24');
        $d = Subnet::parseCidr('172.16.0.0/16');
        $e = IpAddress::parse('192.168.0.0');

        $this->assertTrue($a->equals($b));
        $this->assertFalse($a->equals($c));
        $this->assertFalse($a->equals($d));
        $this->assertFalse($a->equals($e));
    }

    public function testToString(): void
    {
        $cidr = '192.168.0.0/16';
        $subnet = Subnet::parseCidr($cidr);
        $this->assertEquals($cidr, $subnet->getCidr());
        $this->assertEquals($cidr, $subnet->toString());
        $this->assertEquals($cidr, (string)$subnet);

        $cidr = 'a:b:c::/48';
        $subnet = Subnet::parseCidr($cidr);
        $this->assertEquals($cidr, $subnet->getCidr());
        $this->assertEquals($cidr, $subnet->toString());
        $this->assertEquals($cidr, (string)$subnet);
    }

    public function testNamedAddresses(): void
    {
        $subnet = Subnet::parseCidr('192.168.0.9/16');
        $this->assertEquals('192.168.0.9', $subnet->getAddress());
        $this->assertEquals('192.168.0.0', $subnet->getNetworkAddress());
        $this->assertEquals('192.168.0.0', $subnet->getFirstAddress());
        $this->assertEquals('192.168.0.1', $subnet->getRouterAddress());
        $this->assertEquals('192.168.255.255', $subnet->getLastAddress());
        $this->assertEquals('192.168.255.255', $subnet->getBroadcastAddress());
        $this->assertEquals('192.168.255.255', $subnet->getIpv4BroadcastAddress());
        $this->assertEquals('255.255.0.0', $subnet->getSubnetMask());

        $subnet = Subnet::parseCidr('a:b:c::/48');
        $this->assertEquals('a:b:c::', $subnet->getNetworkAddress());
        $this->assertEquals('a:b:c::1', $subnet->getRouterAddress());
        $this->assertEquals('a:b:c:ffff:ffff:ffff:ffff:ffff', $subnet->getLastAddress());
        $this->assertEquals('a:b:c:ffff:ffff:ffff:ffff:ffff', $subnet->getBroadcastAddress());
        $this->assertEquals('ffff:ffff:ffff::', $subnet->getSubnetMask());
    }

    public function testIpv6GetBroadcastAddress(): void
    {
        self::expectException(InvalidArgumentException::class);
        $subnet = Subnet::parseCidr('a:b:c::/48');
        $subnet->getIpv4BroadcastAddress();
    }
}
