<?php

/**
 * SPDX-License-Identifier: MIT
 * Find the full license text at: https://gitlab.com/1of0/php/ip-utils/-/blob/master/LICENSE.md
 */

/** @noinspection PhpUnhandledExceptionInspection */

namespace OneOfZero\IpUtils\Test;

use InvalidArgumentException;
use OneOfZero\IpUtils\BinaryString;
use OneOfZero\IpUtils\IpAddress;
use OneOfZero\IpUtils\ParseException;
use OneOfZero\IpUtils\Subnet;
use ReflectionProperty;
use RuntimeException;

class IpAddressTest extends AbstractTest
{
    /**
     * @dataProvider getValidIpProvider
     * @param string $ip
     */
    public function testIsValidIpAddressValid($ip): void
    {
        $this->assertTrue(
            IpAddress::isValidIpAddress($ip),
            "{$ip} was marked as invalid IP"
        );
    }

    /**
     * @dataProvider getInvalidIpProvider
     * @param string $ip
     */
    public function testIsValidIpAddressInvalid($ip): void
    {
        $this->assertFalse(
            IpAddress::isValidIpAddress($ip),
            "{$ip} was marked as valid IP"
        );
    }

    /**
     * @dataProvider getIpFamilyProvider
     * @param string $ip
     * @param string $family
     */
    public function testGetAddressFamily(string $ip, string $family): void
    {
        $this->assertEquals(
            $family,
            IpAddress::parse($ip)->getAddressFamily(),
            "{$ip} did not register as {$family}"
        );
    }

    /**
     * @dataProvider getCanonicalizationDataProvider
     * @param string $ip
     * @param string $expectedCanonicalForm
     */
    public function testCanonicalizeIpAddress(string $ip, string $expectedCanonicalForm): void
    {
        $this->assertEquals(
            $expectedCanonicalForm,
            IpAddress::canonicalizeIpAddress($ip),
            "{$ip} was not canonicalized to expected {$expectedCanonicalForm}"
        );
    }

    /**
     * @dataProvider getInvalidIpProvider
     * @param string $ip
     */
    public function testCanonicalizeInvalidIpAddress($ip): void
    {
        $this->expectException(ParseException::class);
        IpAddress::canonicalizeIpAddress($ip);
    }

    /**
     * @dataProvider getValidSimpleIpProvider
     * @param string $ip
     */
    public function testConstructorValid($ip): void
    {
        if (inet_pton($ip) !== false) {
            $this->assertNotNull(new IpAddress(new BinaryString(inet_pton($ip))));
        }
    }

    /**
     * @dataProvider getInvalidIpProvider
     * @param string $ip
     */
    public function testConstructorInvalid($ip): void
    {
        $this->expectException(InvalidArgumentException::class);
        new IpAddress(new BinaryString(inet_pton($ip)));
    }

    /**
     * @dataProvider getValidSimpleIpProvider
     * @param string $ip
     */
    public function testParseValid($ip): void
    {
        $this->assertEquals($ip, IpAddress::parse($ip)->toString());
    }

    /**
     * @dataProvider getInvalidIpProvider
     * @param string $ip
     */
    public function testParseInvalid($ip): void
    {
        $this->expectException(ParseException::class);
        IpAddress::parse($ip)->toString();
    }

    /**
     * @dataProvider getValidSimpleIpProvider
     * @param string $ip
     */
    public function testGetIdentifier($ip): void
    {
        $this->assertEquals(bin2hex(inet_pton($ip)), IpAddress::parse($ip)->getIdentifier());
    }

    public function testToStringInvalid(): void
    {
        $ip = IpAddress::parse('127.0.0.1');
        $binaryAddressProperty = new ReflectionProperty($ip, 'binaryAddress');
        $binaryAddressProperty->setAccessible(true);
        $binaryAddressProperty->setValue($ip, new BinaryString(''));
        $this->expectException(RuntimeException::class);
        $ip->toString();
    }

    public function testDebugInfo(): void
    {
        $ip = IpAddress::parse('127.0.0.1');
        $this->assertEquals('127.0.0.1', $ip->__debugInfo()['address']);
        $this->assertEquals(IpAddress::FAMILY_IPV4, $ip->__debugInfo()['family']);
    }

    public function testGetBinaryString(): void
    {
        $this->assertEquals(inet_pton('127.0.0.1'), IpAddress::parse('127.0.0.1')->getBinaryAddress()->toString());
    }

    public function testEquals(): void
    {
        $a = IpAddress::parse('127.0.0.1');
        $b = IpAddress::parse('127.0.0.1');
        $c = IpAddress::parse('127.0.0.2');
        $d = Subnet::parseCidr('127.0.0.0/8');

        $this->assertTrue($a->equals($b));
        $this->assertFalse($a->equals($c));
        $this->assertFalse($a->equals($d));

        $this->assertTrue($a->contains($b));
        $this->assertFalse($a->contains($c));
        $this->assertFalse($a->contains($d));
    }

    public function testGetSubnetForPrefixLength(): void
    {
        $subnet = IpAddress::parse('127.0.0.1')->getSubnetForPrefixLength(8);
        $this->assertInstanceOf(Subnet::class, $subnet);
        $this->assertEquals(8, $subnet->getPrefixLength());
        $this->assertEquals('127.0.0.0', $subnet->getNetworkAddress()->toString());
    }

    public function testExistsInSubnet(): void
    {
        $subnet = Subnet::parseCidr('127.0.0.0/8');
        $ip = IpAddress::parse('127.0.0.1');
        $this->assertTrue($ip->existsInSubnet($subnet));
    }

    /**
     * @dataProvider getCanonicalizationDataProvider
     * @param string $ip
     * @param string $expectedCanonicalForm
     */
    public function testGetCidr(string $ip, string $expectedCanonicalForm): void
    {
        $ip = IpAddress::parse($ip);
        if ($ip->getAddressFamily() === IpAddress::FAMILY_IPV4) {
            $this->assertEquals("{$expectedCanonicalForm}/32", $ip->getCidr());
        } else {
            $this->assertEquals("{$expectedCanonicalForm}/128", $ip->getCidr());
        }
    }
}
