<?php

/**
 * SPDX-License-Identifier: MIT
 * Find the full license text at: https://gitlab.com/1of0/php/ip-utils/-/blob/master/LICENSE.md
 */

namespace OneOfZero\IpUtils\Test;

use OneOfZero\IpUtils\Factory;
use OneOfZero\IpUtils\IpAddress;
use OneOfZero\IpUtils\Subnet;

class FactoryTest extends AbstractTest
{
    public function testParse(): void
    {
        foreach ($this->getValidIpProvider() as $args) {
            $instance = Factory::get()->parse($args[0]);
            $this->assertNotNull($instance);
            $this->assertInstanceOf(IpAddress::class, $instance);
        }

        foreach ($this->getValidCidrProvider() as $args) {
            $instance = Factory::get()->parse($args[0]);
            $this->assertNotNull($instance);
            $this->assertInstanceOf(Subnet::class, $instance);
        }

        foreach ($this->getInvalidCidrProvider() as $args) {
            if (IpAddress::isValidIpAddress($args[0])) {
                // Skip valid IPs in the invalid CIDR test
                continue;
            }
            $instance = Factory::get()->parse($args[0]);
            $this->assertNull($instance);
        }

        foreach ($this->getInvalidIpProvider() as $args) {
            if (Subnet::isValidCidr($args[0])) {
                // Skip valid CIDRs in the invalid IP test
                continue;
            }
            $instance = Factory::get()->parse($args[0]);
            $this->assertNull($instance);
        }
    }
}
