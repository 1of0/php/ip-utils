<?php

/**
 * SPDX-License-Identifier: MIT
 * Find the full license text at: https://gitlab.com/1of0/php/ip-utils/-/blob/master/LICENSE.md
 */

declare(strict_types=1);

namespace OneOfZero\IpUtils;

use InvalidArgumentException;
use RuntimeException;

/**
 * Representation of an IP address with helper methods.
 */
class IpAddress implements NetworkLocationInterface
{
    public const FAMILY_IPV4 = 'ipv4';
    public const FAMILY_IPV6 = 'ipv6';

    public const ADDRESS_LENGTH_BITS = [
        self::FAMILY_IPV4 => 32,
        self::FAMILY_IPV6 => 128,
    ];

    private BinaryString $binaryAddress;

    /**
     * Create an instance of this class.
     *
     * Uses the provided {@link BinaryString} representing an in_addr notation of an IP address.
     *
     * @param BinaryString $binaryAddress
     */
    public function __construct(BinaryString $binaryAddress)
    {
        if (inet_ntop($binaryAddress->toString()) === false) {
            throw new InvalidArgumentException(
                "Provided address 0x{$binaryAddress->toHex()} is not a valid in_addr"
            );
        }
        $this->binaryAddress = $binaryAddress;
    }

    /**
     * Hide the funky stuff.
     *
     * @return array<string, string>|null
     */
    public function __debugInfo(): ?array
    {
        return [
            'address' => $this->toString(),
            'family' => $this->getAddressFamily(),
        ];
    }

    /**
     * Creates an instance of this class given an IP string.
     *
     * If the provided string is not a valid (or not supported) IP address format, an exception will be thrown.
     *
     * @param string $ip
     * @return self
     *
     * @throws ParseException if the provided IP cannot be parsed
     */
    public static function parse(string $ip): self
    {
        $ip = self::sanitizeHumanReadableIp($ip);
        if (!self::isValidIpAddress($ip)) {
            throw new ParseException("{$ip} is not a valid IP address");
        }
        return new self(new BinaryString(inet_pton($ip)));
    }

    /**
     * Returns whether the provided IP string contains a valid IP address.
     *
     * @param string $ip
     * @return bool
     */
    public static function isValidIpAddress(string $ip): bool
    {
        $ip = self::sanitizeHumanReadableIp($ip);
        return inet_pton($ip) !== false;
    }

    /**
     * Returns the canonical form of the provided IP address.
     *
     * For example 127.000.000.001 will be translated into 127.0.0.1. If the provided string is not a valid (or not
     * supported) IP address format, an exception will be thrown.
     *
     * @param string $ip
     * @return string
     *
     * @throws ParseException if the provided IP cannot be parsed
     */
    public static function canonicalizeIpAddress(string $ip): string
    {
        $ip = self::sanitizeHumanReadableIp($ip);
        if (!self::isValidIpAddress($ip)) {
            throw new ParseException("{$ip} is not a valid IP address");
        }
        return inet_ntop(inet_pton($ip));
    }

    /**
     * {@inheritDoc}
     */
    public function getIdentifier(): string
    {
        return $this->binaryAddress->toHex();
    }

    /**
     * Returns a wrapped in_addr.
     *
     * The in_addr will be wrapped in a {@link BinaryString} instance.
     *
     * @return BinaryString
     */
    public function getBinaryAddress(): BinaryString
    {
        return $this->binaryAddress;
    }

    /**
     * Returns the address family of the IP address. For example ipv4 or ipv6.
     *
     * @return string
     */
    public function getAddressFamily(): string
    {
        return (string)array_search($this->binaryAddress->getLength() * 8, self::ADDRESS_LENGTH_BITS);
    }

    /**
     * Returns an object with this IP and the provided prefix length.
     *
     * The returned object will be an instance of {@link Subnet}.
     *
     * @param int $prefixLength
     * @return Subnet
     *
     * @throws ParseException if the prefix length is not valid for this IP address
     */
    public function getSubnetForPrefixLength(int $prefixLength): Subnet
    {
        return new Subnet($this, $prefixLength);
    }

    /**
     * Returns whether this IP exists in the provided object.
     *
     * @param Subnet $subnet
     * @return bool
     */
    public function existsInSubnet(Subnet $subnet): bool
    {
        return $subnet->contains($this);
    }

    /**
     * Returns whether this IP is the same as the IP represented in another object.
     *
     * @param NetworkLocationInterface $location
     * @return bool
     */
    public function equals(NetworkLocationInterface $location): bool
    {
        if ($location instanceof self) {
            return $this->binaryAddress->equals($location->binaryAddress);
        }
        return false;
    }

    /**
     * Returns whether this IP is the same as the IP represented in another object.
     *
     * @param NetworkLocationInterface $location
     * @return bool
     */
    public function contains(NetworkLocationInterface $location): bool
    {
        return $this->equals($location);
    }

    /**
     * Returns the CIDR of this subnet.
     *
     * @return string
     */
    public function getCidr(): string
    {
        $prefixLength = self::ADDRESS_LENGTH_BITS[$this->getAddressFamily()];
        return $this->getSubnetForPrefixLength($prefixLength)->getCidr();
    }

    /**
     * Returns the human readable representation of the IP address.
     *
     * @return string
     */
    public function toString(): string
    {
        return $this->__toString();
    }

    /**
     * Returns the human readable representation of the IP address.
     *
     * @return string
     */
    public function __toString()
    {
        $ipString = inet_ntop($this->binaryAddress->toString());
        if ($ipString === false) {
            throw new RuntimeException("Could not represent 0x{$this->binaryAddress->toHex()} as IP address");
        }
        return (string)$ipString;
    }

    private static function sanitizeHumanReadableIp(string $input): string
    {
        // Skip if not IPv4
        if (strpos($input, '.') === false) {
            return $input;
        }

        // Skip if already a valid IP
        if (inet_pton($input) !== false) {
            return $input;
        }

        // Skip sanitizing if the input contains anything other than dots and digits
        if (preg_match('/^[.\d]+$/', $input) !== 1) {
            return $input;
        }

        $parts = explode('.', $input);

        // Trim off preceding zeroes for each part
        $parts = array_map('intval', $parts);

        if (count($parts) === 4) {
            return implode('.', $parts);
        }

        // If there are less than 4 parts, insert 0 parts before the last provided part
        // Example: [127,1] becomes [127,0,0,1], or [192,168,1] becomes [192,168,0,1]
        $last = array_pop($parts);
        for ($i = 0; $i < (4 - count($parts)); $i++) {
            $parts[] = 0;
        }
        $parts[] = $last;

        return implode('.', $parts);
    }
}
