<?php

/**
 * SPDX-License-Identifier: MIT
 * Find the full license text at: https://gitlab.com/1of0/php/ip-utils/-/blob/master/LICENSE.md
 */

declare(strict_types=1);

namespace OneOfZero\IpUtils;

use InvalidArgumentException;
use OverflowException;
use UnderflowException;

/**
 * Helper class to perform bitwise operations on numbers encoded as binary strings.
 *
 * An example of such binary strings is the packed in_addr representation of IPs that is output by {@link \inet_pton()}.
 */
class BinaryString
{
    private string $string;

    /**
     * Create an instance of this class with the provided string as base.
     *
     * @param string $string
     */
    public function __construct(string $string)
    {
        $this->string = $string;
    }

    /**
     * Creates a mask for the given prefix length and mask size.
     *
     * The mask size must be a multiple of 8, and the prefix length cannot be greater than the mask size.
     *
     * An example mask for prefix length 16 and mask size 32 would be ffff0000.
     *
     * @param int $prefixLength
     * @param int $maskSize
     * @return self
     *
     * @throws InvalidArgumentException if the prefix length or mask size are less than 0, if the prefix length is
     *                                  greater than the mask size, or if the mask size is not a multiple of 8
     */
    public static function createMaskForPrefixLength(int $prefixLength, int $maskSize): self
    {
        if ($prefixLength < 0 || $maskSize < 0) {
            throw new InvalidArgumentException(
                "Prefix length ({$prefixLength}) and mask size ({$maskSize}) must be larger than zero"
            );
        }
        if (($maskSize % 8) !== 0) {
            throw new InvalidArgumentException("Mask size ({$maskSize}) must be a multiple of 8");
        }

        if ($prefixLength > $maskSize) {
            throw new InvalidArgumentException(
                "Prefix length ({$prefixLength}) must be smaller than or equal to the mask size ({$maskSize})"
            );
        }

        $mask = '';
        for ($bitOffset = 0; $bitOffset < $maskSize; $bitOffset += 8) {
            $mask .= self::createMaskingByteForPrefixBits($bitOffset + 8 - $prefixLength);
        }
        return new self($mask);
    }

    /**
     * Creates an instance of this class given the hex representation of the data.
     *
     * @param string $hex
     * @return self
     *
     * @throws InvalidArgumentException if the provided string is not hexadecimal
     */
    public static function fromHex(string $hex): self
    {
        if (!ctype_xdigit($hex)) {
            throw new InvalidArgumentException("String '{$hex}' is not a valid hex string");
        }
        return new self(hex2bin($hex));
    }

    /**
     * Creates an instance of this class given the decimal representation of the data.
     *
     * @param int $integer
     * @return self
     *
     * @throws InvalidArgumentException if the value is a negative number
     */
    public static function fromUnsignedInteger(int $integer): self
    {
        if ($integer < 0) {
            throw new InvalidArgumentException("Value {$integer} is not an unsigned integer");
        }
        $hex = dechex($integer);
        if ((strlen($hex) % 2) !== 0) {
            $hex = '0' . $hex;
        }
        return static::fromHex($hex);
    }

    /**
     * Increments the value of the binary string by the provided amount (1 by default).
     *
     * Note that the length of the binary string is fixed and this method will throw an exception when incrementing
     * will overflow the value.
     *
     * @param int $incrementBy
     * @return self
     *
     * @throws InvalidArgumentException if the increment amount is less than 1
     * @throws OverflowException if the increment will overflow the value
     */
    public function increment(int $incrementBy = 1): self
    {
        if ($incrementBy < 1) {
            throw new InvalidArgumentException("IncrementBy ({$incrementBy}) must be larger than zero");
        }

        $incrementedString = $this->string;
        $incrementLeft = $incrementBy;

        for ($i = $this->getLength() - 1; $i >= 0; $i--) {
            $oldByte = ord($incrementedString[$i]);
            $newByte = ($oldByte + ($incrementLeft & 0xFF)) % 256;

            $incrementedString[$i] = chr($newByte);

            $incrementLeft >>= 8;

            if ($newByte < $oldByte) {
                $incrementLeft++;
            }

            if ($incrementLeft === 0) {
                break;
            }
        }

        if ($incrementLeft > 0) {
            throw new OverflowException(
                sprintf('Overflow adding 0x%s to 0x%s', dechex($incrementBy), $this->toHex())
            );
        }

        return new self($incrementedString);
    }

    /**
     * Decrements the value of the binary string by the provided amount (1 by default).
     *
     * Note that the length of the binary string is fixed and this method will throw an exception when decrementing
     * will underflow the value.
     *
     * @param int $decrementBy
     * @return self
     *
     * @throws InvalidArgumentException if the decrement amount is less than 1
     * @throws UnderflowException if the decrement will underflow the value
     */
    public function decrement(int $decrementBy = 1): self
    {
        if ($decrementBy < 1) {
            throw new InvalidArgumentException("DecrementBy ({$decrementBy}) must be larger than zero");
        }

        $decrementedString = $this->string;
        $decrementLeft = $decrementBy;

        for ($i = $this->getLength() - 1; $i >= 0; $i--) {
            $oldByte = ord($decrementedString[$i]);
            $newByte = (256 + $oldByte - ($decrementLeft & 0xFF)) % 256;

            $decrementedString[$i] = chr($newByte);

            $decrementLeft >>= 8;

            if ($newByte > $oldByte) {
                $decrementLeft++;
            }

            if ($decrementLeft === 0) {
                break;
            }
        }

        if ($decrementLeft > 0) {
            throw new UnderflowException(
                sprintf('Underflow subtracting 0x%s from 0x%s', dechex($decrementBy), $this->toHex())
            );
        }

        return new self($decrementedString);
    }

    /**
     * Performs a bitwise NOT on the string and returns it in a new instance.
     *
     * @return self
     */
    public function bitwiseNot(): self
    {
        // Damned if you do, damned if you don't
        /** @noinspection UnnecessaryCastingInspection */
        return new self((string)~$this->string);
    }

    /**
     * Performs a bitwise NOT on the string and returns it in a new instance. Alias of {@link bitwiseNot()}.
     *
     * @return self
     */
    public function bitwiseInvert(): self
    {
        return $this->bitwiseNot();
    }

    /**
     * Performs a bitwise AND on the string and returns it in a new instance.
     *
     * The provided operand must be a string or BinaryString instance.
     *
     * @param string|self $operand
     * @return self
     *
     * @throws InvalidArgumentException if the operand is not a string nor a BinaryString instance
     */
    public function bitwiseAnd($operand): self
    {
        $this->validateStringArgument($operand, __METHOD__);
        return new self((string)($this->string & (string)$operand));
    }

    /**
     * Performs a bitwise OR on the string and returns it in a new instance.
     *
     * The provided operand must be a string or BinaryString instance.
     *
     * @param string|self $operand
     * @return self
     *
     * @throws InvalidArgumentException if the operand is not a string nor a BinaryString instance
     */
    public function bitwiseOr($operand): self
    {
        $this->validateStringArgument($operand, __METHOD__);
        return new self((string)($this->string | (string)$operand));
    }

    /**
     * Performs a bitwise XOR on the string and returns it in a new instance.
     *
     * The provided operand must be a string or BinaryString instance.
     *
     * @param string|self $operand
     * @return self
     *
     * @throws InvalidArgumentException if the operand is not a string nor a BinaryString instance
     */
    public function bitwiseXor($operand): self
    {
        $this->validateStringArgument($operand, __METHOD__);
        return new self((string)($this->string ^ (string)$operand));
    }

    /**
     * Compares this instance with the provided operand.
     *
     * The provided operand must be a string or BinaryString instance.
     *
     * @param string|self $operand
     * @return int
     *
     * @throws InvalidArgumentException if the operand is not a string nor a BinaryString instance
     */
    public function compare($operand): int
    {
        $this->validateStringArgument($operand, __METHOD__);
        return strcmp($this->string, (string)$operand);
    }

    /**
     * Returns whether the value represented by this instance is greater than the operand.
     *
     * The provided operand must be a string or BinaryString instance.
     *
     * @param string|self $operand
     * @return bool
     *
     * @throws InvalidArgumentException if the operand is not a string nor a BinaryString instance
     */
    public function greaterThan($operand): bool
    {
        $this->validateStringArgument($operand, __METHOD__);
        return $this->compare($operand) > 0;
    }

    /**
     * Returns whether the value represented by this instance is greater than or equal to the operand.
     *
     * The provided operand must be a string or BinaryString instance.
     *
     * @param string|self $operand
     * @return bool
     *
     * @throws InvalidArgumentException if the operand is not a string nor a BinaryString instance
     */
    public function greaterThanOrEqualTo($operand): bool
    {
        $this->validateStringArgument($operand, __METHOD__);
        return $this->compare($operand) >= 0;
    }

    /**
     * Returns whether the value represented by this instance is less than the operand.
     *
     * The provided operand must be a string or BinaryString instance.
     *
     * @param string|self $operand
     * @return bool
     *
     * @throws InvalidArgumentException if the operand is not a string nor a BinaryString instance
     */
    public function lessThan($operand): bool
    {
        $this->validateStringArgument($operand, __METHOD__);
        return $this->compare($operand) < 0;
    }

    /**
     * Returns whether the value represented by this instance is less than or equal to the operand.
     *
     * The provided operand must be a string or BinaryString instance.
     *
     * @param string|self $operand
     * @return bool
     *
     * @throws InvalidArgumentException if the operand is not a string nor a BinaryString instance
     */
    public function lessThanOrEqualTo($operand): bool
    {
        $this->validateStringArgument($operand, __METHOD__);
        return $this->compare($operand) <= 0;
    }

    /**
     * Returns whether the value represented by this instance is equal to the operand.
     *
     * The provided operand must be a string or BinaryString instance.
     *
     * @param string|self $operand
     * @return bool
     *
     * @throws InvalidArgumentException if the operand is not a string nor a BinaryString instance
     */
    public function equals($operand): bool
    {
        $this->validateStringArgument($operand, __METHOD__);
        return $this->string === (string)$operand;
    }

    /**
     * Returns the length of the string.
     *
     * @return int
     */
    public function getLength(): int
    {
        return strlen($this->string);
    }

    /**
     * Returns the hex representation of the string.
     *
     * @return string
     */
    public function toHex(): string
    {
        return bin2hex($this->string);
    }

    /**
     * Returns the decimal representation of the string.
     *
     * @return float|int
     */
    public function toUnsignedInteger()
    {
        return hexdec($this->toHex());
    }

    /**
     * Returns the raw string value
     *
     * @return string
     */
    public function toString(): string
    {
        return $this->string;
    }

    /**
     * Returns the raw string value
     *
     * @return string
     */
    public function __toString()
    {
        return $this->string;
    }

    /**
     * @param mixed  $operand
     * @param string $method
     */
    private function validateStringArgument($operand, string $method): void
    {
        if (is_string($operand) || ($operand instanceof self)) {
            return;
        }
        throw new InvalidArgumentException(
            sprintf('Operand type (%s) not supported for BinaryString::%s()', gettype($operand), $method)
        );
    }

    private static function createMaskingByteForPrefixBits(int $prefixBits): string
    {
        // Translate to a value between 0 and 8 (inclusive)
        $prefixBits = max(0, min($prefixBits, 8));
        return chr(255 << $prefixBits);
    }
}
