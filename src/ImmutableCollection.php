<?php

/**
 * SPDX-License-Identifier: MIT
 * Find the full license text at: https://gitlab.com/1of0/php/ip-utils/-/blob/master/LICENSE.md
 */

declare(strict_types=1);

namespace OneOfZero\IpUtils;

use InvalidArgumentException;

/**
 * Immutable collection.
 *
 * This collection handles implementations of {@link NetworkLocationInterface} such as {@see IpAddress} and
 * {@see Subnet}.
 */
class ImmutableCollection extends AbstractCollection
{
    /**
     * Returns a copy of the collection with the provided location added as item.
     *
     * The location must be an object that implements the {@link NetworkLocationInterface}, or a string that can be
     * parsed by the {@link Factory}.
     *
     * @param NetworkLocationInterface|string $location
     * @return static
     *
     * @throws InvalidArgumentException if the provided location cannot be parsed
     */
    public function withItem($location): self
    {
        $newSet = clone $this;
        $newSet->addLocation($this->resolveLocation($location));
        return $newSet;
    }

    /**
     * Returns a copy of the collection with the provided locations added as items.
     *
     * The locations must be objects that implement the {@link NetworkLocationInterface}, or strings that can be
     * parsed by the {@link Factory}.
     *
     * @param NetworkLocationInterface[]|string[] $locations
     * @return static
     *
     * @throws InvalidArgumentException if the provided locations contain items that cannot be parsed
     */
    public function withItems(array $locations): self
    {
        $newSet = clone $this;
        $newSet->addLocations($this->resolveLocations($locations));
        return $newSet;
    }

    /**
     * Returns a copy of the collection with all redundant items filtered out.
     *
     * For example if the collection contains 192.168.0.0/24 and 192.168.0.1, the filtered collection will only
     * contain a {@link Subnet} instance for 192.168.0.0/24.
     *
     * @return static
     */
    public function withRedundantItemsFilteredOut(): self
    {
        $newSet = clone $this;
        $newSet->filterRedundant();
        return $newSet;
    }

    /**
     * Returns a copy of the collection without the provided location in the collection.
     *
     * The location must be an object that implements the {@link NetworkLocationInterface}, or a string that can be
     * parsed by the {@link Factory}.
     *
     * @param NetworkLocationInterface|string $location
     * @return static
     *
     * @throws InvalidArgumentException if the provided location cannot be parsed
     */
    public function withoutItem($location): self
    {
        $newSet = clone $this;
        $newSet->removeLocation($this->resolveLocation($location));
        return $newSet;
    }

    /**
     * Returns a copy of the collection without the provided locations in the collection.
     *
     * The locations must be objects that implements the {@link NetworkLocationInterface}, or strings that can be
     * parsed by the {@link Factory}.
     *
     * @param NetworkLocationInterface[]|string[] $locations
     * @return static
     *
     * @throws InvalidArgumentException if the provided locations contain items that cannot be parsed
     */
    public function withoutItems(array $locations): self
    {
        $newSet = clone $this;
        $newSet->removeLocations($this->resolveLocations($locations));
        return $newSet;
    }
}
