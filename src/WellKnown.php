<?php

namespace OneOfZero\IpUtils;

class WellKnown
{
    private static ?ImmutableCollection $privateRanges = null;
    private static ?ImmutableCollection $linkLocalRanges = null;
    private static ?ImmutableCollection $multicastRanges = null;
    private static ?ImmutableCollection $localhostRanges = null;

    public static function getIpv4PrivateRanges(): ImmutableCollection
    {
        if (self::$privateRanges === null) {
            self::$privateRanges = (new ImmutableCollection)->withItems([
                '10.0.0.0/8',
                '172.16.0.0/12',
                '192.168.0.0/16',
            ]);
        }
        return self::$privateRanges;
    }

    public static function getIpv4LinkLocalRanges(): ImmutableCollection
    {
        if (self::$linkLocalRanges === null) {
            self::$linkLocalRanges = (new ImmutableCollection)->withItems([
                '169.254.0.0/16',
            ]);
        }
        return self::$linkLocalRanges;
    }

    public static function getIpv4MulticastRanges(): ImmutableCollection
    {
        if (self::$multicastRanges === null) {
            self::$multicastRanges = (new ImmutableCollection)->withItems([
                '224.0.0.0/4',
            ]);
        }
        return self::$multicastRanges;
    }

    public static function getIpv4LocalHostRanges(): ImmutableCollection
    {
        if (self::$localhostRanges === null) {
            self::$localhostRanges = (new ImmutableCollection)->withItems([
                '127.0.0.0/8',
            ]);
        }
        return self::$localhostRanges;
    }
}
