<?php

/**
 * SPDX-License-Identifier: MIT
 * Find the full license text at: https://gitlab.com/1of0/php/ip-utils/-/blob/master/LICENSE.md
 */

declare(strict_types=1);

namespace OneOfZero\IpUtils;

use ArrayIterator;
use Countable;
use InvalidArgumentException;
use IteratorAggregate;
use Traversable;

/**
 * Abstract implementation of a collection
 *
 * The collection holds {@link NetworkLocationInterface} instances.
 *
 * @implements IteratorAggregate<string, NetworkLocationInterface>
 */
abstract class AbstractCollection implements IteratorAggregate, Countable
{
    /**
     * @var array<string, NetworkLocationInterface>
     */
    protected array $items = [];

    protected Factory $factory;

    /**
     * Create an instance of this class with the provided locations as initial set.
     *
     * The locations must be objects that implement the {@link NetworkLocationInterface}, or strings that can be
     * parsed by the {@link Factory}.
     *
     * @param NetworkLocationInterface[]|string[] $locations
     * @param Factory|null                        $factory
     *
     * @throws InvalidArgumentException if the provided locations contain items that cannot be parsed
     */
    public function __construct(array $locations = [], ?Factory $factory = null)
    {
        if ($factory === null) {
            $factory = Factory::get();
        }
        $this->factory = $factory;

        $this->addLocations($this->resolveLocations($locations));
    }

    /**
     * Returns whether the collection contains the provided location.
     *
     * The location must be an object that implements the {@link NetworkLocationInterface}, or a string that can be
     * parsed by the {@link Factory}.
     *
     * @param NetworkLocationInterface|string $location
     * @return bool
     *
     * @throws InvalidArgumentException if the provided location cannot be parsed
     */
    public function contains($location): bool
    {
        return $this->containsLocation($this->resolveLocation($location));
    }

    /**
     * Returns whether the collection contains the provided location. If the collection contains subnets, each subnet
     * is checked whether it contains the provided subnet or address.
     *
     * The location must be an object that implements the {@link NetworkLocationInterface}, or a string that can be
     * parsed by the {@link Factory}.
     *
     * @param NetworkLocationInterface|string $location
     * @return bool
     *
     * @throws InvalidArgumentException if the provided location cannot be parsed
     */
    public function containsNested($location): bool
    {
        $resolvedLocation = $this->resolveLocation($location);

        if ($this->containsLocation($resolvedLocation)) {
            return true;
        }

        foreach ($this->items as $item) {
            if ($item instanceof Subnet) {
                if ($resolvedLocation instanceof IpAddress && $item->containsIpAddress($resolvedLocation)) {
                    return true;
                }
                if ($resolvedLocation instanceof Subnet && $item->containsSubnet($resolvedLocation)) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Returns the items that this collection holds.
     *
     * @return NetworkLocationInterface[]
     */
    public function getItems(): array
    {
        return array_values($this->items);
    }

    /**
     * Returns an iterator to the items.
     *
     * See {@link getItems()}
     *
     * @return Traversable
     */
    public function getIterator(): Traversable
    {
        return new ArrayIterator($this->getItems());
    }

    /**
     * Returns the number of items in the collection.
     *
     * @return int
     */
    public function count(): int
    {
        return count($this->getItems());
    }

    /**
     * Returns the string representations of the items that this collection holds.
     *
     * @return string[]
     */
    public function getStringRepresentations(): array
    {
        return array_values(array_map('strval', $this->items));
    }

    /**
     * Filters out redundant items from the set, e.g. IP addresses that are represented by a subnet.
     */
    protected function filterRedundant(): void
    {
        $unfilteredSet = $this->items;

        $this->items = [];
        foreach ($unfilteredSet as $unfilteredItem) {
            $this->addLocationWithRedundancyFiltering($unfilteredItem);
        }
    }

    /**
     * Returns whether the collection contains the provided location.
     *
     * @param NetworkLocationInterface $location
     * @return bool
     */
    protected function containsLocation(NetworkLocationInterface $location): bool
    {
        return array_key_exists($location->getIdentifier(), $this->items);
    }

    /**
     * Adds the provided location to the collection.
     *
     * @param NetworkLocationInterface $location
     */
    protected function addLocation(NetworkLocationInterface $location): void
    {
        $this->items[$location->getIdentifier()] = $location;
    }

    /**
     * Adds the provided location to the collection if it's not already in it. Also removes items that become
     * redundant by adding the provided location.
     *
     * @param NetworkLocationInterface $location
     */
    protected function addLocationWithRedundancyFiltering(NetworkLocationInterface $location): void
    {
        foreach ($this->items as $item) {
            if ($item->contains($location)) {
                // Already represented by an existing IP or subnet
                return;
            }
            if ($location->contains($item)) {
                // Item will be redundant after adding this location; remove
                $this->removeLocation($item);
            }
        }

        $this->items[$location->getIdentifier()] = $location;
    }

    /**
     * Adds the provided locations to the collection.
     *
     * @param NetworkLocationInterface[] $locations
     */
    protected function addLocations(array $locations): void
    {
        foreach ($locations as $location) {
            $this->addLocation($location);
        }
    }

    /**
     * Removes the provided location from the collection.
     *
     * @param NetworkLocationInterface $location
     */
    protected function removeLocation(NetworkLocationInterface $location): void
    {
        unset($this->items[$location->getIdentifier()]);
    }

    /**
     * Removes the provided locations from the collection.
     *
     * @param NetworkLocationInterface[] $locations
     */
    protected function removeLocations(array $locations): void
    {
        foreach ($locations as $location) {
            $this->removeLocation($location);
        }
    }

    /**
     * Resolves the locations to {@link NetworkLocationInterface} or throws an exception.
     *
     * @param mixed[] $locations
     * @return NetworkLocationInterface[]
     *
     * @throws InvalidArgumentException if the provided locations contain items that cannot be parsed
     */
    protected function resolveLocations(array $locations): array
    {
        $resolved = [];
        foreach ($locations as $location) {
            $resolved[] = $this->resolveLocation($location);
        }
        return $resolved;
    }

    /**
     * Resolves location to an object.
     *
     * Resolves the location to {@link NetworkLocationInterface} or throws an exception.
     *
     * @param mixed $location
     * @return NetworkLocationInterface
     *
     * @throws InvalidArgumentException if the provided location cannot be parsed
     */
    protected function resolveLocation($location): NetworkLocationInterface
    {
        if ($location instanceof NetworkLocationInterface) {
            return $location;
        }

        if (is_string($location)) {
            $parsedLocation = $this->factory->parse($location);
            if ($parsedLocation) {
                return $parsedLocation;
            }
        }

        throw new InvalidArgumentException('Could not parse provided location into a NetworkLocationInterface');
    }
}
