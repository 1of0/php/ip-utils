<?php

/**
 * SPDX-License-Identifier: MIT
 * Find the full license text at: https://gitlab.com/1of0/php/ip-utils/-/blob/master/LICENSE.md
 */

declare(strict_types=1);

namespace OneOfZero\IpUtils;

use InvalidArgumentException;

/**
 * Representation of a subnet with helper methods.
 */
class Subnet implements NetworkLocationInterface
{
    private BinaryString $baseAddress;

    private BinaryString $binaryNetworkAddress;

    private int $prefixLength;

    private BinaryString $prefixMask;

    private BinaryString $hostMask;

    /**
     * Create an instance of this class.
     *
     * Uses the provided {@link IpAddress} instance and prefix length. Will throw an exception if the prefix length
     * is not valid for the given IP address.
     *
     * @param IpAddress $baseAddress
     * @param int       $prefixLength
     *
     * @throws ParseException if the prefix length is not valid for this IP address
     */
    public function __construct(IpAddress $baseAddress, int $prefixLength)
    {
        if (!self::isValidPrefixLengthForIpAddress($baseAddress, $prefixLength)) {
            throw new ParseException("'{$prefixLength}' is not a valid prefix length");
        }

        $this->prefixLength = $prefixLength;

        $maskSize = IpAddress::ADDRESS_LENGTH_BITS[$baseAddress->getAddressFamily()];
        $this->prefixMask = BinaryString::createMaskForPrefixLength($prefixLength, $maskSize);
        $this->hostMask = $this->prefixMask->bitwiseInvert();

        $this->baseAddress = $baseAddress->getBinaryAddress();
        $this->binaryNetworkAddress = $baseAddress->getBinaryAddress()->bitwiseAnd($this->prefixMask);
    }

    /**
     * Hide the funky stuff.
     *
     * @return array<string, string>|null
     */
    public function __debugInfo(): ?array
    {
        // Apparently this doesn't register as an array...
        /** @noinspection MagicMethodsValidityInspection */
        return [
            'cidr' => $this->getCidr(),
        ];
    }

    /**
     * Creates an instance from the provided CIDR.
     *
     * Will throw an exception if the IP cannot be parsed or if the prefix length is invalid.
     *
     * @param string $cidr
     * @return self
     *
     * @throws ParseException if the prefix length is not valid for this IP address, or if the IP address cannot be
     *                        parsed
     */
    public static function parseCidr(string $cidr): self
    {
        [$baseAddress, $prefixLength] = self::splitCidr($cidr);

        if ($baseAddress === null || !IpAddress::isValidIpAddress($baseAddress)) {
            throw new ParseException("'{$baseAddress}' is not a valid address base");
        }

        $ip = IpAddress::parse($baseAddress);

        if (!self::isValidPrefixLengthForIpAddress($ip, $prefixLength)) {
            throw new ParseException("'{$prefixLength}' is not a valid prefix length");
        }

        return new self($ip, (int)$prefixLength);
    }

    /**
     * Returns whether the provided CIDR is valid.
     *
     * @param string $cidr
     * @return bool
     */
    public static function isValidCidr(string $cidr): bool
    {
        [$baseAddress, $prefixLength] = self::splitCidr($cidr);

        if ($baseAddress === null || !IpAddress::isValidIpAddress($baseAddress)) {
            return false;
        }

        $ip = IpAddress::parse($baseAddress);

        if (!self::isValidPrefixLengthForIpAddress($ip, $prefixLength)) {
            return false;
        }

        return true;
    }

    /**
     * {@inheritDoc}
     */
    public function getIdentifier(): string
    {
        return $this->binaryNetworkAddress->toHex() . $this->prefixMask->toHex();
    }

    /**
     * Returns whether this subnet is the same as the subnet represented in another instance.
     *
     * @param NetworkLocationInterface $location
     * @return bool
     */
    public function equals(NetworkLocationInterface $location): bool
    {
        if ($location instanceof self) {
            return $this->prefixLength === $location->prefixLength
                && $this->binaryNetworkAddress->equals($location->binaryNetworkAddress);
        }
        return false;
    }

    /**
     * Returns whether the provided object is in the range of this subnet.
     *
     * @param NetworkLocationInterface $location
     * @return bool
     */
    public function contains(NetworkLocationInterface $location): bool
    {
        if ($location instanceof IpAddress) {
            return $this->containsIpAddress($location);
        }

        if ($location instanceof self) {
            return $this->containsSubnet($location);
        }

        return false;
    }

    /**
     * Returns whether the provided object is in the range of this subnet.
     *
     * @param IpAddress $other
     * @return bool
     */
    public function containsIpAddress(IpAddress $other): bool
    {
        return $other->getBinaryAddress()->getLength() === $this->binaryNetworkAddress->getLength()
            && $other->getBinaryAddress()->bitwiseAnd($this->prefixMask)->equals($this->binaryNetworkAddress);
    }

    /**
     * Returns whether the provided object is in the range of this subnet.
     *
     * @param Subnet $other
     * @return bool
     */
    public function containsSubnet(Subnet $other): bool
    {
        $thisNetwork = $this->binaryNetworkAddress;
        $thisBroadcast = $this->getBroadcastAddress()->getBinaryAddress();
        $otherNetwork = $other->getNetworkAddress()->getBinaryAddress();
        $otherBroadcast = $other->getBroadcastAddress()->getBinaryAddress();

        return $otherNetwork->greaterThanOrEqualTo($thisNetwork) && $otherBroadcast->lessThanOrEqualTo($thisBroadcast);
    }

    /**
     * Returns the IP address that this instance was constructed with.
     *
     * For example, if this instance was created for the CIDR 192.168.1.9/24, the address would be 192.168.1.9.
     *
     * @return IpAddress
     */
    public function getAddress(): IpAddress
    {
        return new IpAddress($this->baseAddress);
    }

    /**
     * Returns the network IP address in the subnet represented by this instance.
     *
     * For example, in the subnet 192.168.1.0/24, the network address would be 192.168.1.0.
     *
     * @return IpAddress
     */
    public function getNetworkAddress(): IpAddress
    {
        return new IpAddress($this->binaryNetworkAddress);
    }

    /**
     * Returns the router IP address in the subnet represented by this instance.
     *
     * For example, in the subnet 192.168.1.0/24, the router address would be 192.168.1.1.
     *
     * @return IpAddress
     */
    public function getRouterAddress(): IpAddress
    {
        return new IpAddress($this->binaryNetworkAddress->increment());
    }

    /**
     * @deprecated Use {@link getIpv4BroadcastAddress()} or {@link getLastAddress()} instead
     *
     * Returns the broadcast IP address in the subnet represented by this instance.
     *
     * For example, in the subnet 192.168.1.0/24, the broadcast address would be 192.168.1.255.
     *
     * @return IpAddress
     */
    public function getBroadcastAddress(): IpAddress
    {
        return $this->getLastAddress();
    }

    /**
     * Returns the broadcast IP address in the subnet represented by this instance.
     *
     * For example, in the subnet 192.168.1.0/24, the broadcast address would be 192.168.1.255.
     *
     * @return IpAddress
     *
     * @throws InvalidArgumentException if the subnet is an IPv6 subnet; IPv6 does not specify network numbers as
     *                                   broadcast addresses.
     */
    public function getIpv4BroadcastAddress(): IpAddress
    {
        if ($this->baseAddress->getLength() * 8 !== IpAddress::ADDRESS_LENGTH_BITS[IpAddress::FAMILY_IPV4]) {
            throw new InvalidArgumentException('IPv6 does not specify broadcast addresses in subnets');
        }

        return $this->getLastAddress();
    }

    /**
     * Returns the first address in the subnet represented by this instance. This is also known as the network address
     * or network prefix.
     *
     * For example, in the subnet 192.168.1.0/24, the network address would be 192.168.1.0.
     *
     * @return IpAddress
     */
    public function getFirstAddress(): IpAddress
    {
        return new IpAddress($this->binaryNetworkAddress);
    }

    /**
     * Returns the last address in the subnet represented by this instance. For IPv4 this is also the broadcast address.
     *
     * For example, in the subnet 192.168.1.0/24, the broadcast address would be 192.168.1.255.
     *
     * @return IpAddress
     */
    public function getLastAddress(): IpAddress
    {
        return new IpAddress($this->binaryNetworkAddress->bitwiseOr($this->hostMask));
    }

    /**
     * Returns the subnet mask notation of the prefix.
     *
     * For example if the prefix length is /24, the subnet mask would be 255.255.255.0.
     *
     * @return IpAddress
     */
    public function getSubnetMask(): IpAddress
    {
        return new IpAddress($this->prefixMask);
    }

    /**
     * Returns the prefix length of this subnet.
     *
     * @return int
     */
    public function getPrefixLength(): int
    {
        return $this->prefixLength;
    }

    /**
     * Returns the CIDR of this subnet.
     *
     * @return string
     */
    public function getCidr(): string
    {
        return "{$this->getAddress()->toString()}/{$this->prefixLength}";
    }

    /**
     * Returns the CIDR of this subnet.
     *
     * @return string
     */
    public function toString(): string
    {
        return $this->getCidr();
    }

    /**
     * Returns the CIDR of this subnet.
     *
     * @return string
     */
    public function __toString()
    {
        return $this->getCidr();
    }

    /**
     * @param string $cidr
     * @return array<?string>
     */
    private static function splitCidr(string $cidr): array
    {
        $pieces = explode('/', $cidr);
        return [$pieces[0], $pieces[1] ?? null];
    }

    /**
     * @param IpAddress       $ip
     * @param string|int|null $prefixLength
     * @return bool
     */
    private static function isValidPrefixLengthForIpAddress(IpAddress $ip, $prefixLength): bool
    {
        if ($prefixLength === null) {
            return false;
        }

        $maxPrefixLength = IpAddress::ADDRESS_LENGTH_BITS[$ip->getAddressFamily()];
        return is_numeric($prefixLength) && $prefixLength >= 0 && $prefixLength <= $maxPrefixLength;
    }
}
