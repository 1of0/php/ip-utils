<?php

/**
 * SPDX-License-Identifier: MIT
 * Find the full license text at: https://gitlab.com/1of0/php/ip-utils/-/blob/master/LICENSE.md
 */

declare(strict_types=1);

namespace OneOfZero\IpUtils;

use InvalidArgumentException;

class ParseException extends InvalidArgumentException
{
}
