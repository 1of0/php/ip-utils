#!/bin/bash

[[ ! -e /.dockerenv ]] && exit 0

set -xe

curl --location --output phpDocumentor.phar https://github.com/phpDocumentor/phpDocumentor/releases/download/v3.0.0-rc/phpDocumentor.phar
mv phpDocumentor.phar /usr/local/bin/phpdocumentor
chmod +x /usr/local/bin/phpdocumentor
